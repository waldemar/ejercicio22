import socket
import random
PORT = 8095
IP = "127.0.0.1"
MAX_OPEN_REQUESTS = 5
NUMERO_CONEXIONES = 0
numero_aleatorio = random.randint(0, 20)
premio = False

def process_client(clientsocket):
    print(clientsocket)
    if(premio == False):
     send_message = f"Bienvenidos chicos a mi servidor\n hasta ahora hemos recibido {NUMERO_CONEXIONES} conexiones"
    else:
     send_message = f"Bienvenido has sido la conexión {NUMERO_CONEXIONES} premiada"


    # utf8 supports all lanaguages chars
    # Serializing the data to be transmitted
    send_bytes = str.encode(send_message)
    # We must write bytes, not a string
    clientsocket.send(send_bytes)
    clientsocket.close()


# create an INET, STREAMing socket
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# bind the socket to a public host, and a well-known port
# hostname = socket.gethostname()
# Let's use better the local interface name
hostname = IP
try:
    serversocket.bind((hostname, PORT))
    # become a server socket
    # MAX_OPEN_REQUESTS connect requests before refusing outside connections
    serversocket.listen(MAX_OPEN_REQUESTS)

    while True:
        # accept connections from outside
        print("Waiting for connections at %s %i" % (hostname, PORT))
        (clientsocket, address) = serversocket.accept()
        NUMERO_CONEXIONES +=1 
        print(numero_aleatorio)
        if(NUMERO_CONEXIONES == numero_aleatorio):
           premio = True
        else:
            premio = False
            
        # now do something with the clientsocket
        # in this case, we'll pretend this is a non threaded server
        process_client(clientsocket)

except socket.error:
    print("Problemas using port %i. Do you have permission?" % PORT)
